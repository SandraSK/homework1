const randomPlus = require('./randomPlus');
const random = require('./random');

jest.mock('./random');

random.mockReturnValueOnce(99);
random.mockReturnValueOnce(1);

describe('randomPlus', function() {
  it ('1 + 99 (random) = 100', function() {
    expect(randomPlus(1)).toBe(100);
  });

  it ('5 + 1 (random) = 6', function() {
    expect(randomPlus(5)).toBe(6);
  });
});
const user = require('./user');

describe('user', function() {
  it('input cannot be string', function() {
    expect(function() {
      user('person');
    }).toThrow('id needs to be integer');
  });
});
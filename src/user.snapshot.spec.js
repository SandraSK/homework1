const user = require('./user');

describe('user', function() {
  it('user id = 1', function() {
    expect(user(1)).toMatchSnapshot();  
  });
  it('user id = 56', function() {
    expect(user(56)).toMatchSnapshot();  
  });
  it('user id = 1345', function() {
    expect(user(1345)).toMatchSnapshot();  
  });
});